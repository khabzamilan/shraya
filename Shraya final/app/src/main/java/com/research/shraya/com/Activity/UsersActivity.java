package com.research.shraya.com.Activity;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.research.shraya.com.FirebaseMethods.firebasemethods;
import com.research.shraya.com.Models.Policeusers;
import com.research.shraya.com.Models.myadapter;
import com.research.shraya.com.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class UsersActivity extends Fragment implements myadapter.onbuttonclick {

    private DatabaseReference reference;
    private RecyclerView recyclerView;
    private ArrayList<Policeusers> list;
    private myadapter adapter;
    private String type="";
    private firebasemethods methods;
    private FirebaseAuth auth;


    public UsersActivity() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_users, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView=(RecyclerView)view.findViewById(R.id.myRecycleviewusers);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        methods=new firebasemethods(getContext());
        auth=FirebaseAuth.getInstance();
        reference= FirebaseDatabase.getInstance().getReference().child("Users");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list =new ArrayList<Policeusers>();
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()) {
                    Policeusers police=dataSnapshot1.getValue(Policeusers.class);
//                    firebasemethods methods=new firebasemethods(getContext());
//                    String type=methods.findtypeofusers();
                  //  police.setFlag(false);
                            list.add(police);

                }
                Log.i("TAGto get police verify", "onDataChange: " + type );
                adapter=new myadapter(getActivity(),list,1);
                recyclerView.setAdapter(adapter);
                adapter.setonbuttonclickedlistener(UsersActivity.this);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }

        });
    }
    @Override
    public void onbuttonclicked(int position) {
        Policeusers police=list.get(position);

        if(police.getHeropoints()==null){
            police.setHeropoints("0");
        }
        int points=Integer.parseInt(police.getHeropoints())+10;
            reference.child(police.getUID()).child("heropoints").setValue(Integer.toString(points));
    }
}
