package com.research.shraya.com.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.research.shraya.com.FirebaseMethods.firebasemethods;
import com.research.shraya.com.R;
import com.squareup.picasso.Picasso;

import static android.app.Activity.RESULT_OK;

public class policeinfo extends Fragment {

    private static final int PICK_IMAGE_REQUEST = 1;
    private EditText username,email,remail,address,age;
    private Button next,choose,chose1;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef ;
    private ImageView mImageView,miview;
    private Uri mImageUri,murI2;
    private int i=0;
    private StorageReference mStorageRef;
    String mail;
    String type;
    String user_id;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_policeinfo,container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        username=(EditText)view.findViewById(R.id.name);
        email=view.findViewById(R.id.email);
        address=view.findViewById(R.id.address);
        next=view.findViewById(R.id.Request);
        mAuth=FirebaseAuth.getInstance();
        mImageView=view.findViewById(R.id.image_view);
        choose=view.findViewById(R.id.choosefile);
        age=view.findViewById(R.id.age);
        mStorageRef = FirebaseStorage.getInstance().getReference("uploads");
        user_id=mAuth.getCurrentUser().getUid();
        myRef=FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child("type");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                type =dataSnapshot.getValue(String.class);
                Log.i("verification", "onDataChange: i am inside to check if citizen" + type);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(type.equals("Police")) {
                    uploadFile();
                }else {

                    Toast.makeText(getActivity(),"You have SignUp as Citizen", Toast.LENGTH_SHORT).show();

                }
            }
        });

        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i=0;
                openFileChooser();

            }
        });
    }
    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            if(i==0){
                mImageUri = data.getData();
                Picasso.with(getActivity()).load(mImageUri).into(mImageView);
            }
            else {
                murI2 = data.getData();
                Picasso.with(getActivity()).load(murI2).into(miview);
            }
        }
    }
    private String getFileExtension(Uri uri) {
        ContentResolver cR = getActivity().getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    private void uploadFile() {
        if (mImageUri != null) {
            final StorageReference fileReference = mStorageRef.child(System.currentTimeMillis()
                    + "." + getFileExtension(mImageUri));
//            final StorageReference fileReference1 = mStorageRef.child(System.currentTimeMillis()
//                    + "." + getFileExtension(murI2));

            fileReference.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    //here
                    Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                    while (!urlTask.isSuccessful());
                    Uri downloadUrl = urlTask.getResult();
                    String use=username.getText().toString();
                    String ages=age.getText().toString();
                    String addr=address.getText().toString();
                    mail=email.getText().toString();
                    final String sdownload_url = String.valueOf(downloadUrl);
                    firebasemethods me=new firebasemethods(getContext());
                    me.addpolice(use,mail,addr,ages,sdownload_url);
                }}).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
//
//            mUploadTask = fileReference.putFile(mImageUri)
//                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                        @Override
//                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                            Toast.makeText(getContext(), "Upload successful", Toast.LENGTH_LONG).show();
//                            firebaseUri = taskSnapshot.getStorage().getDownloadUrl();
//                            myRef=database.getReference();
//                            String use=username.getText().toString();
//                            String ages=age.getText().toString();
//                            String addr=address.getText().toString();
//                            mail=email.getText().toString();
////                            Policeusers trans=new Policeusers();
////                            trans.setName(use);
////                            trans.setBadgenumber(mail);
////                            trans.setDepartment(addr);
////                            trans.setAge(ages);
////                            trans.setFlag(false);
////                            trans.setImage1(firebaseUri.toString());
////                            //  trans.setImage2(firebaseUri1.toString());
////                            //mAuth = FirebaseAuth.getInstance();
////                            myRef.child("PoliceInfo").child(mail).setValue(trans);
//                            firebasemethods me=new firebasemethods(getContext());
//                            me.addpolice(use,mail,addr,ages,firebaseUri.toString());
//                        }
//                    })
//                    .addOnFailureListener(new OnFailureListener() {
//                        @Override
//                        public void onFailure(@NonNull Exception e) {
//                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
//                        }
//                    });
//
//            mUploadTask=fileReference1.putFile(murI2).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                @Override
//                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                    firebaseUri1 = taskSnapshot.getStorage().getDownloadUrl();
//
//                    myRef.child("PoliceInfo").child(mail).child("image2").setValue(firebaseUri1);
//                }
//            })
//                    .addOnFailureListener(new OnFailureListener() {
//                        @Override
//                        public void onFailure(@NonNull Exception e) {
//                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
//                        }
//                    });
//            myRef=database.getReference();
//            String use=username.getText().toString();
//            String ages=age.getText().toString();
//            String addr=address.getText().toString();
//            String mail=email.getText().toString();
//            Policeusers trans=new Policeusers();
//            trans.setName(use);
//            trans.setBadgenumber(mail);
//            trans.setDepartment(addr);
//            trans.setAge(ages);
//            trans.setFlag(false);
//            trans.setImage1(firebaseUri.toString());
//            trans.setImage2(firebaseUri1.toString());
//            //mAuth = FirebaseAuth.getInstance();
//            myRef.child("PoliceInfo").child(mail).setValue(trans);
        } else {
            Toast.makeText(getActivity(), "No file selected", Toast.LENGTH_SHORT).show();
        }
    }
}
