package com.research.shraya.com.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.research.shraya.com.FirebaseMethods.firebasemethods;
import com.research.shraya.com.R;
import com.squareup.picasso.Picasso;

public class signup1 extends AppCompatActivity {
    private ImageView imageView;
    View viewline;
//    private TextView textView;
    private Button button,confirm;
    private EditText batch,email,password,cpassword,username,emergencynumber;
    private RadioButton selectedtype;
    private RadioButton selectedgender;
    private RadioGroup gender;
    private RadioGroup type;
    private String sex,types;
    private Uri mImageUri;
    private Button editimage;
    private StorageReference mStorageRef;
    private static final int PICK_IMAGE_REQUEST = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup1);
        batch=(EditText) findViewById(R.id.batchno);
        mStorageRef = FirebaseStorage.getInstance().getReference("uploads");
        batch.setVisibility(View.INVISIBLE);
        email=(EditText)findViewById(R.id.email);
        password=(EditText)findViewById(R.id.passwords);
        cpassword=(EditText)findViewById(R.id.confirmpassword);
        emergencynumber=(EditText)findViewById(R.id.Emergencynumber);
        username=(EditText)findViewById(R.id.usernames);
        gender=(RadioGroup)findViewById(R.id.gender);
        type=(RadioGroup)findViewById(R.id.type);
        viewline = findViewById(R.id.batch_line);
        viewline.setVisibility(View.INVISIBLE);
        confirm=(Button)findViewById(R.id.confirm);
        selectedgender=(RadioButton)findViewById(R.id.male);
        selectedtype=(RadioButton)findViewById(R.id.citizen);
        selectedgender.setChecked(true);
        selectedtype.setChecked(true);
        imageView=(ImageView)findViewById(R.id.propic);
        editimage=(Button)findViewById(R.id.edit_photo);
//        imageView = (ImageView) findViewById(R.id.signup_back);
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openMainPage();
//            }
//        });

        editimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });

        button=(Button) findViewById(R.id.haveacc);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLogin();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mImageUri != null) {
                    final StorageReference fileReference = mStorageRef.child(System.currentTimeMillis()
                            + "." + getFileExtension(mImageUri));


                    fileReference.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            //here
                            Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                            while (!urlTask.isSuccessful());
                            Uri downloadUrl = urlTask.getResult();
                    String user = username.getText().toString();
                    String pass = password.getText().toString();
                    String emails = email.getText().toString();
                    String confirmpass = cpassword.getText().toString();
                    String emergencyNumber=emergencynumber.getText().toString();
                    final String sdownload_url = String.valueOf(downloadUrl);
                    if (selectedgender.isChecked()) {
                        sex = "Male";
                    } else {
                        sex = "Female";
                    }
                    if (selectedtype.isChecked()) {
                        types = "Citizen";
                    } else {
                        types = "Police";
                    }
                    if (pass != null && confirmpass != null) {
                        if (pass.equals(confirmpass)) {
                            if (!(user.isEmpty() && emails.isEmpty())) {
                                firebasemethods method = new firebasemethods(signup1.this);
                                method.registernewusers(emails, confirmpass, sex, types, user, sdownload_url,emergencyNumber);
                            } else {

                            }

                        } else {


                        }


                    } else {
                    }
                        }}).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(signup1.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }




    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
                mImageUri = data.getData();
                Picasso.with(this).load(mImageUri).into(imageView);
        }
    }
    private String getFileExtension(Uri uri) {
        ContentResolver cR = this.getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    public void openLogin(){
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);

        }



//    public void openMainPage(){
//        Intent intent = new Intent(this, MainActivity.class);
//        startActivity(intent);
//    }



    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        String str="";
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.police:
                if(checked)
                    batch.setVisibility(View.VISIBLE);
                    viewline.setVisibility(View.VISIBLE);
                break;
            case R.id.citizen:
                if(checked)
                    batch.setVisibility(View.INVISIBLE);
                viewline.setVisibility(View.INVISIBLE);
                break;
        }
        Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
    }
}

