package com.research.shraya.com.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.research.shraya.com.FirebaseMethods.firebasemethods;
import com.research.shraya.com.Models.Policeusers;
import com.research.shraya.com.Models.myadapter;
import com.research.shraya.com.R;

import java.util.ArrayList;

public class police_verification extends Fragment implements myadapter.onbuttonclick {

    DatabaseReference reference,mDataRef,ref;
    RecyclerView recyclerView;
    ArrayList<Policeusers> list;
    myadapter adapter;
    String type="";
    firebasemethods methods;
    FirebaseAuth auth;
    String user_id;
    boolean flag;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_police_verification,container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView=(RecyclerView)view.findViewById(R.id.myRecycleview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        methods=new firebasemethods(getContext());
        auth=FirebaseAuth.getInstance();
        user_id=auth.getCurrentUser().getUid();
        //type=methods.findtypeofusers();
        mDataRef=FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child("type");
        mDataRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                type =dataSnapshot.getValue(String.class);
                Log.i("verification", "onDataChange: i am inside to check if citizen" + type);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        reference= FirebaseDatabase.getInstance().getReference().child("PoliceInfo");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list =new ArrayList<Policeusers>();
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()) {
                    Policeusers police=dataSnapshot1.getValue(Policeusers.class);
//                    firebasemethods methods=new firebasemethods(getContext());
//                    String type=methods.findtypeofusers();
                        if(type.equals("Citizen")){
                            if(police.isFlag()){
                                list.add(police);
                            }
                        }else{
                            if(police.getUID().equals(user_id)){
                                flag=police.isFlag();
                            }
                            list.add(police);
                        }
                }
                Log.i("TAGto get police verify", "onDataChange: " + type );
                adapter=new myadapter(getActivity(),list,0);
                recyclerView.setAdapter(adapter);
                adapter.setonbuttonclickedlistener(police_verification.this);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
//
//        if(type.equals("Police")) {
//            ref = FirebaseDatabase.getInstance().getReference().child("PoliceInfo").child(user_id).child("flag");
//            ref.addValueEventListener(new ValueEventListener() {
//                @Override
//                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                    flag = dataSnapshot.getValue(Boolean.class);
//                    if(flag){
//                        Log.i("Flf is true","Trueture");
//                    }else {
//                        Log.i("flag is false","Why it is false");
//                    }
//                    Log.i("verification", "onDataChange: i am inside to check if citizen" + type);
//                }
//                @Override
//                public void onCancelled(@NonNull DatabaseError databaseError) {
//                }
//            });
//        }
    }
    @Override
    public void onbuttonclicked(int position) {
        Policeusers police=list.get(position);
        if(flag){
        reference.child(police.getUID()).child("flag").setValue(true);
    }else{
        Toast.makeText(getActivity(),"You cant Verify Police Now", Toast.LENGTH_SHORT).show();
    }
    }
}
