package com.research.shraya.com.Models;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.research.shraya.com.R;

import java.util.ArrayList;


public class formlistadapter extends RecyclerView.Adapter<formlistadapter.viewHolder> {
    Context context;
    ArrayList<Forms> formlist;
    onbuttonclickedlistener onclick;

    public formlistadapter(Context context, ArrayList<Forms> formlist) {
        this.context = context;
        this.formlist = formlist;
    }

    public interface onbuttonclickedlistener{
        void onbuttonclicked(int position);
        void ongetlocationclicked(int position);

    }
    public void setonbuttonclickedlistener(onbuttonclickedlistener onbutton){
        onclick=onbutton;
    }
    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new viewHolder(LayoutInflater.from(context).inflate(R.layout.formlist,viewGroup,false));
    }
    @Override
    public void onBindViewHolder(@NonNull final viewHolder viewholder, int i) {
       // viewholder.submittedname.setText(formlist.get(i).get);
        viewholder.submittedname.setText(formlist.get(i).getName());
        viewholder.crimetype.setText(formlist.get(i).getTypeofcrime());
//        viewholder.victemage.setText(formlist.get(i).getAge());
//        viewholder.victimsex.setText(formlist.get(i).getSex());
        viewholder.phonenumber.setText(formlist.get(i).getPhonenumber());
        viewholder.victemage.setText(formlist.get(i).getDescription());
        viewholder.location.setText(formlist.get(i).getAddress());
        viewholder.date.setText(formlist.get(i).getDate());

        if(formlist.get(i).getURI()!=null) {
            Glide.with(context)
                    .load(formlist.get(i).getURI())
                    .asBitmap()
                    .centerCrop()
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            viewholder.imgform.setImageBitmap(resource);
                        }
                    });
        }
        if(!formlist.get(i).isValidate()) {
            viewholder.verify.setVisibility(View.VISIBLE);
            viewholder.veriy.setVisibility(View.INVISIBLE);
        }
        else{
            viewholder.verify.setVisibility(View.INVISIBLE);
            viewholder.veriy.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return formlist.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        TextView submittedname,phonenumber,victemage,veriy,crimetype,date,location;
        Button verify,getlocation;
        ImageView imgform;

        public viewHolder(@NonNull View itemView) {
            super(itemView);
            submittedname=(TextView)itemView.findViewById(R.id.submmittedname);
            phonenumber=(TextView)itemView.findViewById(R.id.phonenumber);
            victemage=(TextView)itemView.findViewById(R.id.Victimage);
            veriy=(TextView)itemView.findViewById(R.id.veri);
            crimetype=(TextView)itemView.findViewById(R.id.typeofCrime);
            date=(TextView)itemView.findViewById(R.id.date);
            location=(TextView) itemView.findViewById(R.id.location);
            verify=(Button)itemView.findViewById(R.id.verifyform);
            getlocation=(Button)itemView.findViewById(R.id.getmap);
            imgform=(ImageView) itemView.findViewById(R.id.formimg);
            verify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onclick!=null){
                        int position=getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            onclick.onbuttonclicked(position);
                        }
                    }
                }
            });
            getlocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onclick!=null){
                        int position=getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            onclick.ongetlocationclicked(position);
                        }
                    }
                }
            });
        }
    }
}
