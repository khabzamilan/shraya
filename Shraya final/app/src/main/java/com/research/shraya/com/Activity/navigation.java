package com.research.shraya.com.Activity;

import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.util.Log;
import android.view.MenuItem;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.research.shraya.com.FirebaseMethods.firebasemethods;
import com.research.shraya.com.MainActivity;
import com.research.shraya.com.R;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import javax.xml.transform.Templates;

public class navigation extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private FirebaseAuth mAuth;
    private ImageView iview;
    private TextView name,gmail;
    private DatabaseReference reference,mDataRef,ref;
    private FirebaseAuth auth;
    private String user_id,gm,names,im;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        mAuth=FirebaseAuth.getInstance();
        Toolbar toolbar = findViewById(R.id.toolbar);
        iview=(ImageView)findViewById(R.id.imageView) ;
        name=(TextView)findViewById(R.id.navname);
        gmail=(TextView)findViewById(R.id.navgmail);
        setSupportActionBar(toolbar);

        auth=FirebaseAuth.getInstance();
        user_id=auth.getCurrentUser().getUid();

        reference= FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child("gmail");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                gm =dataSnapshot.getValue(String.class);
                Log.i("verification", "onDataChange: i am inside to check if citizen" + gm);

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }


        });

        mDataRef= FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child("name");
        mDataRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                 names =dataSnapshot.getValue(String.class);
                Log.i("verification", "onDataChange: i am inside to check if citizen" + names);


            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
            });
        ref= FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child("uri");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                 im =dataSnapshot.getValue(String.class);
                Log.i("verification", "onDataChange: i am inside to check if citizen" + im);

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        firebasemethods method=new firebasemethods(navigation.this);
        String type=method.findtypeofusers();
        if(type.equals("Citizen")){
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_verify_police).setVisible(false);
        }
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        name.setText(names);
        gmail.setText(gm);

        Glide.with(navigation.this)
                .load(im)
                .asBitmap()
                .centerCrop()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        iview.setImageBitmap(resource);
                    }
                });



        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_dashboard) {
            Intent in=new Intent(navigation.this,MainActivity.class);
            startActivity(in);
        } else if (id == R.id.nav_map) {
         Intent in=new Intent(navigation.this,MapsActivity.class);
         startActivity(in);
        } else if (id == R.id.nav_report) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new report()).commit();
        } else if (id == R.id.users) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new UsersActivity()).commit();
        }else if (id == R.id.nav_notification) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new list_notification()).commit();
        } else if (id == R.id.PoliceInfo) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new policeinfo()).commit();
        }else if (id == R.id.nav_signout) {
            mAuth.signOut();
            Intent in=new Intent(navigation.this, MainActivity.class);
            startActivity(in);
        }
        else if (id == R.id.message) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new HelpMessage()).commit();
        }
        else if (id == R.id.nav_verify_police){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
            new police_verification()).commit();
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
