package com.research.shraya.com.Activity;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.research.shraya.com.FirebaseMethods.firebasemethods;
import com.research.shraya.com.Models.Forms;
import com.research.shraya.com.Models.UserLocation;
import com.research.shraya.com.Models.formlistadapter;
import com.research.shraya.com.R;
import com.research.shraya.com.Sqlite.DatabaseAccess;
import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    public static final int MY_PERMISSION_REQUEST=0;

    private GoogleMap mMap;
    private DatabaseReference reference;
    private Button but;
    private FirebaseAuth mAuth;
    private FusedLocationProviderClient mfusedLocationProviderclient;
    private Location mylocations;
    private static final int REQUEST_CODE=101;
    public static final String TAG="MapActivity";
    public static final String Fine_Location= android.Manifest.permission.ACCESS_FINE_LOCATION;
    public static final String COURSE_Location= android.Manifest.permission.ACCESS_COARSE_LOCATION;
    public static final int Location_Permission_RequestCode=234;
    public static final float Default_Zoom=15f;
    private boolean mlocation_Request=false;
    private DatabaseReference ref,databaseref,mDataRef;
    private DatabaseAccess access;
    private ArrayList<UserLocation> list;
    private String title="Threat";
    private String number;

    LatLng lt,mylocation,lt1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        mAuth=FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance().getReference().child("PoliceLocation");
        access=new DatabaseAccess(this);
        mfusedLocationProviderclient = LocationServices.getFusedLocationProviderClient(this);
       SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
       access.open();
       //list=access.getlocation();
       access.close();
        mapFragment.getMapAsync(this);
        firebasemethods me=new firebasemethods(this);
        me.addlocation(27.6934, 85.3207,true);
        getLocationPermission();
        but=(Button)findViewById(R.id.button);
        databaseref = FirebaseDatabase.getInstance().getReference("Freeforms");
        checkpermission();

        mDataRef=FirebaseDatabase.getInstance().getReference().child("Users").child(mAuth.getCurrentUser().getUid()).child("Phonenumber");
        mDataRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                number =dataSnapshot.getValue(String.class);
                Log.i("verification", "onDataChange: i am inside to check if citizen" + number);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });


        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(lt!=null){
                firebasemethods me=new firebasemethods(MapsActivity.this);
                ref = FirebaseDatabase.getInstance().getReference().child("Users").child(mAuth.getCurrentUser().getUid()).child("name");
                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot != null) {
                           String flag = dataSnapshot.getValue(String.class);
                           Log.i("verification", "onDataChange: i am inside to check if citizen" + flag);
                            SmsManager smsManager = SmsManager.getDefault();
                            smsManager.sendTextMessage("9803028810", null, "Help me I am this coordinates"+ lt.latitude + lt.longitude, null, null);
                            //smsManager.sendTextMessage(number, null, "Help me I am this coordinates"+ lt.latitude + lt.longitude, null, null);

                            firebasemethods me=new firebasemethods(MapsActivity.this);
                            me.addlocationforhelp(mylocation,flag);

                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });}
                else{
                    Toast.makeText(MapsActivity.this,"Wait Awhile", Toast.LENGTH_SHORT).show();
                }
//                databaseref = FirebaseDatabase.getInstance().getReference().child("Users").child(mAuth.getCurrentUser().getUid()).child("phonenumber");
//                databaseref.addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                        if (dataSnapshot != null) {
//                             number = dataSnapshot.getValue(String.class);
//                           // Log.i("verification", "onDataChange: i am inside to check if citizen" + flag);
//                        }
//                    }
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//                    }
//                });
            }
        });
    }

    private void getdeviceLocation(){
        Log.d(TAG,"Getting Your Location");
        mfusedLocationProviderclient= LocationServices.getFusedLocationProviderClient(this);
        try{
            if(mlocation_Request){
                Task location=mfusedLocationProviderclient.getLastLocation();
                location.addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            mylocation = new LatLng(location.getLatitude(), location.getLongitude());
                            firebasemethods fb=new firebasemethods(MapsActivity.this);
                            fb.addlocation(location.getLatitude(),location.getLongitude(),false);
                        movecamera(mylocation,
                                Default_Zoom);

                        }
                    }
                });
            }
        }catch (SecurityException e){
            Log.e(TAG,"getDeviceLocation:secuirty Exceptoin:" + e.getMessage() );
        }
    }
    private void movecamera(LatLng latlng,float zoom){

        final CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latlng)      // Sets the center of the map to Mountain View
                .zoom(13)                   // Sets the zoom
                .bearing(90)                // Sets the orientation of the camera to east
                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                .build();                   //
        mMap.addMarker(new MarkerOptions().position(latlng).title("Me"));
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                return true;
            }
        });
    }
    private void initMap(){
        SupportMapFragment mapFragment=(SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(MapsActivity.this);
    }

    private void getLocationPermission(){
        String[] permissions={Fine_Location,COURSE_Location};
        if(ContextCompat.checkSelfPermission(this.getApplicationContext(),Fine_Location)== PackageManager.PERMISSION_GRANTED){
            if(ContextCompat.checkSelfPermission(this.getApplicationContext(),COURSE_Location)==PackageManager.PERMISSION_GRANTED){
                mlocation_Request=true;
                initMap();
            }else{
                ActivityCompat.requestPermissions(this,permissions,Location_Permission_RequestCode);
            }
        }else{
            ActivityCompat.requestPermissions(this,permissions,Location_Permission_RequestCode);
        }

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        getdeviceLocation();
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //loc =new ArrayList<UserLocation>();
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()) {
                    UserLocation userloca=dataSnapshot1.getValue(UserLocation.class);
                    if(userloca.isStatus()) {
                        BitmapDescriptor subwayBitmapDescriptor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW);

                        lt = new LatLng(userloca.getLatitude(), userloca.getLongitude());
                        mMap.addMarker(new MarkerOptions()
                                .position(lt)
                                .title("Police").icon(subwayBitmapDescriptor));

                        //        .icon(bitmapDescriptorFromVector(MapsActivity.this,R.drawable.ic_accessibility_black_24dp,0)));
                     }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        databaseref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1: dataSnapshot.getChildren()) {
                    Forms p = dataSnapshot1.getValue(Forms.class);
                    if (p.isValidate()) {
                        BitmapDescriptor subwayBitmapDescriptor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE);

                        lt1 = new LatLng(p.getLatitude(), p.getLongitude());
                        mMap.addMarker(new MarkerOptions()
                                .position(lt1)
                                .title("Crime").icon(subwayBitmapDescriptor));

                        //        .icon(bitmapDescriptorFromVector(MapsActivity.this, R.drawable.crime,1)));
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
              //  Toast.makeText(this, "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });

//        for (UserLocation loc:list){
//            LatLng latlnt= new LatLng(loc.getLatitude(),loc.getLongitude());
//            if(loc.equals("Robbery")){
//
//                title="Robbery";
//            }else if(loc.equals("Kidnapping")){
//                title="Kidnapping";
//            }
//            else if(loc.equals("Murder")){
//                title="Murder";
//            }
//            else if(loc.equals("Fights")){
//                title="Quarrel/Fights";
//            }
//            else if(loc.equals("Rape")){
//                title="Rape";
//            }else if(loc.equals("Sexual assult")){
//                title="Sexual Harassment";
//            }
//            mMap.addMarker(new MarkerOptions().position(latlnt).title(title));
//        }
    }
    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorDrawableResourceId,int i) {
        Drawable background = null;
        if(i==0) {
             background = ContextCompat.getDrawable(context, R.drawable.ic_accessibility_black_24dp);
        }
        else{
            background = ContextCompat.getDrawable(context, R.drawable.crime);
        }
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        vectorDrawable.setBounds(40, 20, vectorDrawable.getIntrinsicWidth() + 40, vectorDrawable.getIntrinsicHeight() + 20);
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
    public void checkpermission(){
        if(ContextCompat.checkSelfPermission(this,Manifest.permission.SEND_SMS)!= PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.SEND_SMS)){
            }else{
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.SEND_SMS},MY_PERMISSION_REQUEST);
            }
        }
    }
}
