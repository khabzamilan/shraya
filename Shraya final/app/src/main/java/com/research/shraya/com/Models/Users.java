package com.research.shraya.com.Models;

public class Users {

    private String name;
    private String gmail;
    private String type;
    private boolean flag;
    private String UID;
    private String URI;
    private String sex;
    private String heropoints;
    private String Phonenumber;

    public String getPhonenumber() {
        return Phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        Phonenumber = phonenumber;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getURI() {
        return URI;
    }
    public void setURI(String URI) {
        this.URI = URI;
    }

    public String getHeropoints() {
        return heropoints;
    }
    public void setHeropoints(String heropoints) {
        this.heropoints = heropoints;
    }
    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGmail() {
        return gmail;
    }

    public void setGmail(String gmail) {
        this.gmail = gmail;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
