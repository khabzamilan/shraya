package com.research.shraya.com.Models;

public class Policeusers extends Users {



    private String age;
    private String department;
    private String Badgenumber;
    private String image1,image2;

    public String getAge() {
        return age;
    }
    public void setAge(String age) {
        this.age = age;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getDepartment() {
        return department;
    }



    public void setDepartment(String department) {
        this.department = department;
    }

    public String getBadgenumber() {
        return Badgenumber;
    }

    public void setBadgenumber(String badgenumber) {
        Badgenumber = badgenumber;
    }
}
