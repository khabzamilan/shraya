package com.research.shraya.com.Activity;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.research.shraya.com.Models.Forms;
import com.research.shraya.com.Models.UserLocation;
import com.research.shraya.com.Models.formlistadapter;
import com.research.shraya.com.R;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class list_notification extends Fragment implements formlistadapter.onbuttonclickedlistener {

    private ArrayList<Forms> list;
    private DatabaseReference reference,mDataRef,ref;
    private int total;
    private RecyclerView recyclerView;
    private formlistadapter adapter;
    private String type;
    private String user_id;
    private FirebaseAuth auth;
    private boolean flag=false;
    private Location loc,loc1;
    private ArrayList<Forms> nearbyincident;
    private Button nearby;
    private Geocoder geocoder;
    private List<Address> addresses;
    private String streetname;
    private TextView riskvalue;
    private LatLng lst;
    int currenttotal;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_list_notification,container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loc=new Location("Point A");
        loc1=new Location("Point B");
        nearbyincident=new ArrayList<Forms>();
        geocoder = new Geocoder(getActivity(), Locale.getDefault());
        recyclerView=(RecyclerView)view.findViewById(R.id.myRecycleviewform);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        riskvalue=(TextView)view.findViewById(R.id.riskvalue);
        auth=FirebaseAuth.getInstance();
        user_id=auth.getCurrentUser().getUid();
        reference = FirebaseDatabase.getInstance().getReference().child("PoliceLocation").child(user_id);
        nearby=(Button)view.findViewById(R.id.filter);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //loc =new ArrayList<UserLocation>();
                UserLocation userloca=dataSnapshot.getValue(UserLocation.class);
//                lt = new LatLng(userloca.getLatitude(), userloca.getLongitude());
                loc.setLatitude(userloca.getLatitude());
                loc.setLongitude(userloca.getLongitude());
                try {
                    addresses = geocoder.getFromLocation(userloca.getLatitude(), userloca.getLongitude(), 1);
                    streetname=addresses.get(0).getLocality();
                    lst=new LatLng(userloca.getLatitude(),userloca.getLongitude());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        mDataRef=FirebaseDatabase.getInstance().getReference().child("Users").child(user_id).child("type");
        mDataRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                type =dataSnapshot.getValue(String.class);
                Log.i("verification", "onDataChange: i am inside to check if citizen" + type);
                if(type.equals("Police")) {
                    ref = FirebaseDatabase.getInstance().getReference().child("PoliceInfo").child(user_id).child("flag");
                    ref.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot != null) {
                                flag = dataSnapshot.getValue(boolean.class);
                                Log.i("verification", "onDataChange: i am inside to check if citizen" + type);
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        reference = FirebaseDatabase.getInstance().getReference("Freeforms");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list = new ArrayList<Forms>();
                total=(int)dataSnapshot.getChildrenCount();
                for(DataSnapshot dataSnapshot1: dataSnapshot.getChildren())
                {
                    Forms p = dataSnapshot1.getValue(Forms.class);
                    loc1.setLatitude(p.getLatitude());
                    loc1.setLongitude(p.getLongitude());

                    double distance=loc.distanceTo(loc1);

                    if(distance<=100){
                        nearbyincident.add(p);
                    }

//                    try {
//                        TimeUnit.MINUTES.sleep(1);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    if(type.equals("Citizen")){
//                        if(p.isValidate()){
                            list.add(p);
//                        }
//                    }else{
//                        list.add(p);
//                    }
                    adapter =new formlistadapter(getActivity(),list);
                     currenttotal=nearbyincident.size();
                    recyclerView.setAdapter(adapter);
                    adapter.setonbuttonclickedlistener(list_notification.this);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });
        nearby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter = new formlistadapter(getActivity(), nearbyincident);
                recyclerView.setAdapter(adapter);
                adapter.setonbuttonclickedlistener(list_notification.this);
                if (lst != null) {
                    String locationis = "Risk Calculations: " + "You are at " + "Latitude:" + lst.latitude + " Longitude:" + lst.longitude;
                    if (total / currenttotal < total / 2) {
                        locationis = locationis + " You have high safety in this location";
                    } else if (total / currenttotal > currenttotal / 2) {
                        locationis = locationis + " You have low safety in this location";

                    } else {
                        locationis = locationis + " You have average safety in this location";
                    }
                    riskvalue.setText(locationis);
                }else {
                    Toast.makeText(getContext(),"Wait Awhile", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    @Override
    public void onbuttonclicked(int position) {
        Forms form = list.get(position);
        if (flag||type.equals("Police")) {
            reference.child(form.getUID()).child("validate").setValue(true);
        }else{
            Toast.makeText(getActivity(),"You cant Verify Events", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void ongetlocationclicked(int position) {
        Forms form =list.get(position);
        Intent detailintent=new Intent(getContext(),MapsActivity2.class);
        detailintent.putExtra("latitude",form.getLatitude());
        detailintent.putExtra("longitude",form.getLongitude());
        startActivity(detailintent);
    }
}

