package com.research.shraya.com.Models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.research.shraya.com.R;

import java.util.ArrayList;

public class messagedapter extends RecyclerView.Adapter<messagedapter.viewHolder> {
    Context context;
    ArrayList<UserLocation> formlist;
    messagedapter.onbuttonclickedlistener onclick;

    public messagedapter(Context context, ArrayList<UserLocation> formlist) {
        this.context = context;
        this.formlist = formlist;
    }

    public interface onbuttonclickedlistener{
        void onbuttonclicked(int position);
    }
    public void setonbuttonclickedlistener(messagedapter.onbuttonclickedlistener onbutton){
        onclick=onbutton;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new viewHolder(LayoutInflater.from(context).inflate(R.layout.message,viewGroup,false));
    }
    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position){
        holder.text.setText("I am " + formlist.get(position).getName());
        holder.desc.setText("Help me! I am in Trouble");
    }
    @Override
    public int getItemCount() {
        return formlist.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        TextView text,desc;
        Button but;
        public viewHolder(@NonNull View itemView) {
            super(itemView);
            text=(TextView)itemView.findViewById(R.id.name);
            but=(Button)itemView.findViewById(R.id.help);
            desc=(TextView) itemView.findViewById(R.id.description);
            but.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onclick!=null){
                        int position=getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            onclick.onbuttonclicked(position);
                        }
                    }
                }
            });
        }
    }
}

