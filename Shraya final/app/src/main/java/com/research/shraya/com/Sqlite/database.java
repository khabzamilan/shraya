package com.research.shraya.com.Sqlite;

import android.content.Context;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;


public class database extends SQLiteAssetHelper {


    private static final String DATABASE_NAME = "CrimeDataNewspaper.db";
    private static final int DATABASE_VERSION = 1;

    public database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
}

