package com.research.shraya.com.FirebaseMethods;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;


import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.research.shraya.com.Activity.Login;
import com.research.shraya.com.Activity.MapsActivity;
import com.research.shraya.com.Activity.navigation;
import com.research.shraya.com.Activity.police_verification;
import com.research.shraya.com.Models.Forms;
import com.research.shraya.com.Models.Policeusers;
import com.research.shraya.com.Models.UserLocation;
import com.research.shraya.com.Models.Users;
import com.research.shraya.com.Models.myadapter;

import java.util.ArrayList;

public class firebasemethods {
    public static final String TAG="DatabaseActivity";
    private FirebaseDatabase mfirebase;
    private FirebaseAuth mAuth;
    private DatabaseReference mDataRef;
    private Context mcontext;
    private String user_id;
    private String trype;


    public firebasemethods(Context context){
        mAuth=FirebaseAuth.getInstance();
        mcontext=context;
        mfirebase=FirebaseDatabase.getInstance();
        mDataRef=mfirebase.getReference();
     if(mAuth.getCurrentUser()!=null){
            user_id=mAuth.getCurrentUser().getUid();
         Log.i(TAG, "firebasemethods: notnull");
        }
     else{
         Log.i(TAG, "firebasemethods: null");
     }
    }

    public void registernewusers(final String email1, String password1, final String sex, final String types, final String name, final String URI,final String number){
        mAuth.createUserWithEmailAndPassword(email1, password1) .addOnCompleteListener( new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "createUserWithEmail:success");
                    FirebaseUser user1=mAuth.getCurrentUser();
                    //updateUI(user1);
                    user_id=user1.getUid();
                    sendVerificationEmail(name,email1,types,sex,true,URI,number);
                    //  check=true;
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "createUserWithEmail:failure", task.getException());
                    Toast.makeText(mcontext, "Authentication failed.",
                            Toast.LENGTH_SHORT).show();
                    //  check=false;

                    //updateUI(null);

                }

            }
        });
        //  return check;
    }



    public void loginvalidate(String email, String Password){
                if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(Password)) {
                    mAuth.signInWithEmailAndPassword(email, Password).addOnCompleteListener((Activity) mcontext, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(mcontext, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            } else {
                                FirebaseUser user=mAuth.getCurrentUser();
                                try{
                                    if(user.isEmailVerified()){
                                        Log.d(TAG, "onComplete: success. email is verified.");
                                        //TO DO
                                        Intent intent = new Intent(mcontext, navigation.class );
                                        mcontext.startActivity(intent);
                                        ((Activity) mcontext).finish();
                                    }else{
                                        Toast.makeText(mcontext, "Email is not verified \n check your email inbox.", Toast.LENGTH_SHORT).show();
//                                        mProgressBar.setVisibility(View.GONE);
//                                        mPleaseWait.setVisibility(View.GONE);
                                        mAuth.signOut();
                                    }
                                }catch (NullPointerException e){
                                    Log.e(TAG, "onComplete: NullPointerException: " + e.getMessage() );
                                }
                            }

                        }
                    });
                } else {
                    Toast.makeText(mcontext,"Fill the parameters", Toast.LENGTH_SHORT).show();
                }
            }
    public void sendVerificationEmail(final String name, final String email, final String type, final String sex, boolean flag, final String badgenumber,final String number){
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user != null){
            user.sendEmailVerification()
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                String uid=user.getUid();
                                addusers(name,email,type,sex,true,badgenumber,uid,number);
                                Intent in=new Intent(mcontext, Login.class);
                                mcontext.startActivity(in);

                            }else{
                                Toast.makeText(mcontext, "couldn't send verification email.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }
    public String findtypeofusers(){
//        final String[] type = new String[1];
//        mDataRef=FirebaseDatabase.getInstance().getReference().child("UsersActivity").child("Citizen").child(user_id).child("type");
//        mDataRef.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()) {
//                     type[0] =dataSnapshot1.getValue(String.class)!=null?dataSnapshot1.getValue(String.class):"hey";
//                     Log.i("TAG",type[0]);
//                }
//
//            }
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//            }
//        });
        trype=user_id;
            mDataRef=FirebaseDatabase.getInstance().getReference().child("UsersActivity").child(user_id).child("type");
            mDataRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        trype =dataSnapshot.getValue(String.class);
                    Log.i("verification", "onDataChange: i am inside to check if citizen" + trype);
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });
            Log.i("finaltype", trype);
        return trype;
    }
    public void addusers(String name, String email,String type,String sex, boolean flag,String badgenumber,String uid,String number){
            Users user =new Users();
            user.setFlag(flag);
            user.setGmail(email);
            // user.setLoc(loc);
            user.setSex(sex);
            user.setName(name);
            user.setURI(badgenumber);
            user.setUID(uid);
            user.setType(type);
            user.setHeropoints("0");
            user.setPhonenumber(number);
//            if(type.equals("Citizen")){
//                    mDataRef.child("UsersActivity").child("Citizen").child(uid).setValue(user);
//                }
//            else if(type.equals("Police")){
//                puser.setFlag(flag);
//                puser.setGmail(email);
//                // user.setLoc(loc);
//                puser.setName(name);
//                //pusers.setUse(user);
//                puser.setURI(badgenumber);
//                puser.setSex(sex);
                    mDataRef.child("Users").child(uid).setValue(user);
             //   }
    }
    public void addform(String name,String typecrime,String date,String sex, String Address, String phonenumber,double lat,double log,String uri,String description){
        Forms form=new Forms();
        form.setName(name);
        form.setAddress(Address);
        form.setDate(date);
        //form.setLoc(loc);
        form.setSex(sex);
        form.setPhonenumber(phonenumber);
        form.setTypeofcrime(typecrime);
        form.setValidate(false);
        form.setLatitude(lat);
        form.setLongitude(log);
        Log.i("TAG for image", uri);
        form.setURI(uri);
        form.setDescription(description);
       // form.setUID(user_id);
        String key= mDataRef.child("Freeforms").push().getKey();
        form.setUID(key);
        mDataRef.child("Freeforms").child(key).setValue(form);
        Log.i("Fire", "Firebasedone");
        Toast.makeText(mcontext,"Requested", Toast.LENGTH_SHORT).show();

    }
    public void addlocation(double lat, double log,boolean status){
        UserLocation location=new UserLocation();
        location.setLongitude(log);
        location.setLatitude(lat);
        location.setStatus(status);
        mDataRef.child("PoliceLocation").child(user_id).setValue(location);
    }
    public void addlocationforhelp(LatLng lat,String name){
        UserLocation user=new UserLocation();
        user.setLatitude(lat.latitude);
        user.setLongitude(lat.longitude);
        user.setName(name);
        mDataRef.child("DataofHelp").push().setValue(user);
        Toast.makeText(mcontext,"Message Has been Sent", Toast.LENGTH_SHORT).show();
    }
    public void addpolice(String use,String mail,String addr,String ages,String firebaseUri){
        Policeusers trans=new Policeusers();
        trans.setName(use);
        trans.setBadgenumber(mail);
        trans.setDepartment(addr);
        trans.setAge(ages);
        trans.setFlag(false);
        trans.setImage1(firebaseUri);
        trans.setUID(user_id);
        //  trans.setImage2(firebaseUri1.toString());
        //mAuth = FirebaseAuth.getInstance();
        mDataRef.child("PoliceInfo").child(user_id).setValue(trans);
        Toast.makeText(mcontext,"Request Hasbeen Sent", Toast.LENGTH_SHORT).show();
    }
//    public void addrequest(String uid,boolean value){
//        Forms fo=new Forms();
//        fo.setUID(uid);
//        fo.setValidate(value);
//        mDataRef.child("Formrequest").push().setValue(fo);
//    }
//
//    public void addpolicerequest(String uid,boolean value){
//        Policeusers use=new Policeusers();
//        use.setUID(uid);
//        use.setFlag(value);
//        mDataRef.child("PoliceRequest").setValue(use);
//    }
}
