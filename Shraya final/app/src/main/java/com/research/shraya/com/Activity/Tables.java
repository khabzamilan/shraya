package com.research.shraya.com.Activity;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.research.shraya.com.Models.Forms;
import com.research.shraya.com.R;
import com.research.shraya.com.Sqlite.DatabaseAccess;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Tables extends Fragment {
    TableLayout layouts,layouts1,layouts2,layouts3,layouts4,layouts5,layouts6;

    ArrayList<Forms> form1;


    public Tables() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tables, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DatabaseAccess access=DatabaseAccess.getInstance(getContext());
        access.open();
        form1=new ArrayList<Forms>();

        layouts=(TableLayout)view.findViewById(R.id.layout);
        layouts1=(TableLayout)view.findViewById(R.id.layout1);
        layouts2=(TableLayout)view.findViewById(R.id.layout2);
        layouts3=(TableLayout)view.findViewById(R.id.layout3);
        layouts4=(TableLayout)view.findViewById(R.id.layout4);
        layouts5=(TableLayout)view.findViewById(R.id.layout5);
        layouts6=(TableLayout)view.findViewById(R.id.layout6);

        form1=access.getQuotes("Murder");
        additemintable(layouts,form1);


        form1=access.getQuotes("Sexual assult");
        additemintable(layouts1,form1);

        form1=access.getQuotes("Kidnapping");
        additemintable(layouts2,form1);

        form1=access.getQuotes("Fights");
        additemintable(layouts3,form1);


        form1=access.getQuotes("Robbery");
        additemintable(layouts4,form1);


        form1=access.getQuotes("Rape");
        additemintable(layouts5,form1);

        form1=access.getQuotes("Threat");
        additemintable(layouts6,form1);
        access.close();
    }
    public void additemintable(TableLayout lay, ArrayList<Forms> data){
        for(Forms form: data){
            TableRow tr =  new TableRow(getContext());
            TextView c1 = new TextView(getContext());
            c1.setText(form.getAddress());
            TextView c2 = new TextView(getContext());
            c2.setText(form.getDate());
            tr.addView(c1);
            tr.addView(c2);
            lay.addView(tr);
        }
    }
}