package com.research.shraya.com.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.research.shraya.com.FirebaseMethods.firebasemethods;
import com.research.shraya.com.R;

public class Login extends AppCompatActivity{
    private Button button;
    private Button button1;;


    private EditText username,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        button = (Button) findViewById(R.id.login_butt);
        button1 = (Button) findViewById(R.id.signup_butt);
        username=(EditText) findViewById(R.id.username);
        password=(EditText) findViewById(R.id.password);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSignin();
            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSignup();
            }
        });
    }

    public void openSignup(){
        Intent intent = new Intent(this, signup1.class);

        startActivity(intent);
    }



        public void openSignin(){
            String user=username.getText().toString();
            String pass=password.getText().toString();
            if(user.isEmpty()&&pass.isEmpty()){
            }else{
                firebasemethods method=new firebasemethods(Login.this);
                method.loginvalidate(user,pass);
            }
//            Intent intent = new Intent(this, navigation.class);
//            startActivity(intent);
        }
    }

