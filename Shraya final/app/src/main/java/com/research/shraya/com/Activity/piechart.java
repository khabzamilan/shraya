package com.research.shraya.com.Activity;


import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.research.shraya.com.R;
import com.research.shraya.com.Sqlite.DatabaseAccess;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;

/**
 * A simple {@link Fragment} subclass.
 */
public class piechart extends Fragment {

    PieChartView pieChartView,view1,view2,view3,view4,view5,view6;
    private int total;
    private double percentage;

    public piechart() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_piechart, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        pieChartView = view.findViewById(R.id.chart);
        view1 = view.findViewById(R.id.chart1);
        view2 = view.findViewById(R.id.chart2);
        view3 = view.findViewById(R.id.chart3);
        view4 = view.findViewById(R.id.chart4);
        view5 = view.findViewById(R.id.chart5);
        view6 = view.findViewById(R.id.chart6);

        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getContext());
        databaseAccess.open();
        int[] values = databaseAccess.getnumbers("Robbery");
        Log.d("Fire", Integer.toString(values[0]));
        int[] values1 = databaseAccess.getnumbers("Sexual assult");
        int[] values2 = databaseAccess.getnumbers("Kidnapping");
        int[] values3 = databaseAccess.getnumbers("Fights");
        int[] values4 = databaseAccess.getnumbers("Murder");
        int[] values5 = databaseAccess.getnumbers("Rape");
        int[] values6 = databaseAccess.getnumbers("Threat");


        databaseAccess.close();

        List pieData = new ArrayList<>();
        total = findtotal(values);
        percentage = findpercentage(total, values[0]);
        pieData.add(new SliceValue(values[0], Color.BLUE).setLabel("Kathmandu-"
                + Double.toString(percentage) + "%"));
        percentage = findpercentage(total, values[1]);
        pieData.add(new SliceValue(values[1], Color.GRAY).setLabel("Bhaktapur-" +
                Double.toString(percentage) + "%"));
        percentage = findpercentage(total, values[2]);
        pieData.add(new SliceValue(values[2], Color.RED).setLabel("Lalitpur-" +
                Double.toString(percentage) + "%"));
        PieChartData pieChartData = new PieChartData(pieData);
        pieChartData.setHasLabels(true).setValueLabelTextSize(14);
        pieChartData.setHasCenterCircle(true).setCenterText1("Robbery").setCenterText1FontSize(20).setCenterText1Color(Color.parseColor("#ffffff"));
        pieChartView.setPieChartData(pieChartData);


        List pieData1 = new ArrayList<>();

        total = findtotal(values1);
        percentage = findpercentage(total, values1[0]);
        pieData1.add(new SliceValue(values1[0], Color.BLUE).setLabel("Kathmandu-"
                + Double.toString(percentage) + "%"));
        percentage = findpercentage(total, values1[1]);
        pieData1.add(new SliceValue(values1[1], Color.GRAY).setLabel("Bhaktapur-" +
                Double.toString(percentage) + "%"));
        percentage = findpercentage(total, values1[2]);
        pieData1.add(new SliceValue(values1[2], Color.RED).setLabel("Lalitpur-" +
                Double.toString(percentage) + "%"));
        PieChartData pieChartData1 = new PieChartData(pieData1);
        pieChartData1.setHasLabels(true).setValueLabelTextSize(14);
        pieChartData1.setHasCenterCircle(true).setCenterText1("Sexual Assault").setCenterText1FontSize(20).setCenterText1Color(Color.parseColor("#ffffff"));
        view1.setPieChartData(pieChartData1);

        List pieData2 = new ArrayList<>();
        total = findtotal(values2);
        percentage = findpercentage(total, values2[0]);
        pieData2.add(new SliceValue(values2[0], Color.BLUE).setLabel("Kathmandu-"
                + Double.toString(percentage) + "%"));
        percentage = findpercentage(total, values2[1]);
        pieData2.add(new SliceValue(values2[1], Color.GRAY).setLabel("Bhaktapur-" +
                Double.toString(percentage) + "%"));
        percentage = findpercentage(total, values2[2]);
        pieData2.add(new SliceValue(values2[2], Color.RED).setLabel("Lalitpur-" +
                Double.toString(percentage) + "%"));
        PieChartData pieChartData2 = new PieChartData(pieData1);
        pieChartData2.setHasLabels(true).setValueLabelTextSize(14);
        pieChartData2.setHasCenterCircle(true).setCenterText1("Kidnapping").setCenterText1FontSize(20).setCenterText1Color(Color.parseColor("#ffffff"));
        view2.setPieChartData(pieChartData2);

        List pieData3 = new ArrayList<>();
        total = findtotal(values3);
        percentage = findpercentage(total, values3[0]);

        pieData3.add(new SliceValue(values3[0], Color.BLUE).setLabel("Kathmandu-"
                + Double.toString(percentage) + "%"));
        percentage = findpercentage(total, values3[1]);
        pieData3.add(new SliceValue(values3[1], Color.GRAY).setLabel("Bhaktapur-" +
                Double.toString(percentage) + "%"));
        percentage = findpercentage(total, values3[2]);
        pieData3.add(new SliceValue(values3[2], Color.RED).setLabel("Lalitpur-" +
                Double.toString(percentage) + "%"));
        PieChartData pieChartData3 = new PieChartData(pieData1);
        pieChartData3.setHasLabels(true).setValueLabelTextSize(14);
        pieChartData3.setHasCenterCircle(true).setCenterText1("Fights").setCenterText1FontSize(20).setCenterText1Color(Color.parseColor("#ffffff"));
        view3.setPieChartData(pieChartData3);

        List pieData4 = new ArrayList<>();
        total = findtotal(values4);
        percentage = findpercentage(total, values4[0]);

        pieData4.add(new SliceValue(values4[0], Color.BLUE).setLabel("Kathmandu-"
                + Double.toString(percentage) + "%"));
        percentage = findpercentage(total, values4[1]);
        pieData4.add(new SliceValue(values4[1], Color.GRAY).setLabel("Bhaktapur-" +
                Double.toString(percentage) + "%"));
        percentage = findpercentage(total, values4[2]);
        pieData4.add(new SliceValue(values4[2], Color.RED).setLabel("Lalitpur-" +
                Double.toString(percentage) + "%"));
        PieChartData pieChartData4 = new PieChartData(pieData4);
        pieChartData4.setHasLabels(true).setValueLabelTextSize(14);
        pieChartData4.setHasCenterCircle(true).setCenterText1("Murder").setCenterText1FontSize(20).setCenterText1Color(Color.parseColor("#ffffff"));
        view4.setPieChartData(pieChartData4);


        List pieData5 = new ArrayList<>();
        total = findtotal(values5);
        percentage = findpercentage(total, values5[0]);
        pieData5.add(new SliceValue(values5[0], Color.BLUE).setLabel("Kathmandu-"
                + Double.toString(percentage) + "%"));
        percentage = findpercentage(total, values5[1]);
        pieData5.add(new SliceValue(values5[1], Color.GRAY).setLabel("Bhaktapur-" +
                Double.toString(percentage) + "%"));
        percentage = findpercentage(total, values5[2]);
        pieData5.add(new SliceValue(values5[2], Color.RED).setLabel("Lalitpur-" +
                Double.toString(percentage) + "%"));
        PieChartData pieChartData5 = new PieChartData(pieData5);
        pieChartData5.setHasLabels(true).setValueLabelTextSize(14);
        pieChartData5.setHasCenterCircle(true).setCenterText1("Rape").setCenterText1FontSize(20).setCenterText1Color(Color.parseColor("#ffffff"));
        view5.setPieChartData(pieChartData5);

        List pieData6 = new ArrayList<>();
        total = findtotal(values6);
        percentage = findpercentage(total, values6[0]);

        pieData6.add(new SliceValue(values6[0], Color.BLUE).setLabel("Kathmandu-"
                + Double.toString(percentage) + "%"));
        percentage = findpercentage(total, values6[1]);
        pieData6.add(new SliceValue(values6[1], Color.GRAY).setLabel("Bhaktapur-" +
                Double.toString(percentage) + "%"));
        percentage = findpercentage(total, values6[2]);
        pieData6.add(new SliceValue(values6[2], Color.RED).setLabel("Lalitpur-" +
                Double.toString(percentage) + "%"));
        PieChartData pieChartData6 = new PieChartData(pieData6);
        pieChartData6.setHasLabels(true).setValueLabelTextSize(14);
        pieChartData6.setHasCenterCircle(true).setCenterText1("Threat").setCenterText1FontSize(20).setCenterText1Color(Color.parseColor("#ffffff"));
        view6.setPieChartData(pieChartData6);
    }

    public int findtotal(int[] values) {
        int value = values[0] + values[1] + values[2];
        return value;
    }

    public double findpercentage(int total, int a) {
        double t = (a * 100) / total;
        return t;
    }
}