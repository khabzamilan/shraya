package com.research.shraya.com;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.net.Uri;
import android.os.Bundle;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.research.shraya.com.Activity.Login;
import com.research.shraya.com.Activity.Tables;
import com.research.shraya.com.Activity.navigation;
import com.research.shraya.com.Activity.piechart;
import com.research.shraya.com.FirebaseMethods.firebasemethods;
import com.research.shraya.com.Models.Forms;
import com.research.shraya.com.Sqlite.DatabaseAccess;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ArrayList<Forms> list;
    private DatabaseReference reference;
    private FirebaseAuth mAuth;
    private Button button;
    private TextView mTextMessage;
    private DatabaseAccess databaseAccess ;
    private piechart piecharts;
    private Tables table;
    private BottomNavigationView bottomview;
    private firebasemethods methods;



//    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
//            = new BottomNavigationView.OnNavigationItemSelectedListener() {
//
//        @Override
//        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//            switch (item.getItemId()) {
//                case R.id.navigation_home:
//                    mTextMessage.setText(R.string.home);
//                    return true;
//                case R.id.navigation_dashboard:
//                    mTextMessage.setText(R.string.title_dashboard);
//                    return true;
//                case R.id.navigation_notifications:
//                    mTextMessage.setText(R.string.title_notifications);
//                    return true;
//            }
//            return false;
//        }
//    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth=FirebaseAuth.getInstance();
        databaseAccess = DatabaseAccess.getInstance(this);
        piecharts=new piechart();
        table=new Tables();
        bottomview=(BottomNavigationView)findViewById(R.id.nav_view);
        methods=new firebasemethods(this);
//        methods.addlocation(27.6931, 85.2807,true);
//        methods.addlocation(27.6954, 85.3039,true);
//        methods.addlocation(27.6984, 85.3039,true);
//        methods.addlocation(27.6924, 85.3039,true);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        mTextMessage = findViewById(R.id.message);
        setfragment(piecharts);

      //  navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        bottomview.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                case R.id.navigation_home:
                    setfragment(piecharts);
                    return true;
                case R.id.navigation_dashboard:
                    Intent browserintent=new Intent(Intent.ACTION_VIEW, Uri.parse("https://projectshraya.carto.com/builder/47098d0f-99a5-40a4-8f06-7c635a36b378/embed"));
                    startActivity(browserintent);
                    //setfragment();
                    return true;
                case R.id.navigation_notifications:
                    setfragment(table);

                    return true;
            }
            return false;
            }
        });
        reference = FirebaseDatabase.getInstance().getReference("Freeforms");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list = new ArrayList<Forms>();
                for(DataSnapshot dataSnapshot1: dataSnapshot.getChildren())
                {
                    Forms p = dataSnapshot1.getValue(Forms.class);
                        if(p.isValidate()){
                            list.add(p);
                            Log.i("checkvalue" , p.getUID());
                        }
                }
                databaseAccess.open();
                databaseAccess.openforread();
                databaseAccess.insertdata(list);
                databaseAccess.close();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(MainActivity.this, "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });

        button =(Button) findViewById(R.id.first_page_login);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLogin();
            }
        });
    }
    public  void openLogin(){
        if(mAuth.getCurrentUser()!=null){
            Intent intent = new Intent(this, navigation.class);
            startActivity(intent);
        }
        else{
            Intent intent = new Intent(this, Login.class);
            startActivity(intent);
        }
    }
    private void setfragment(Fragment fragment){
        FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.firstpage,fragment);
        ft.commit();
    }
}


