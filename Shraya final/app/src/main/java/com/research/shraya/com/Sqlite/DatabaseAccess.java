package com.research.shraya.com.Sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import android.util.Log;

import com.research.shraya.com.Models.Forms;
import com.research.shraya.com.Models.UserLocation;


public class DatabaseAccess {
    public static final String TAG= "1";
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;
    private final String tablename= "crime_data";
    private final String column1="Crime_type";
    private final String column2="Location";
    private final String column3="Date";
    private final String column4="ID";

    /**
     * Private constructor to aboid object creation from outside classes.
     *
     * @param context
     */
    public DatabaseAccess(Context context) {
        this.openHelper = new database(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }
    public void openforread(){
        this.database=openHelper.getReadableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    /**
     * Read all quotes from the database.
     *
     * @return a List of quotes
     */
    public ArrayList<Forms> getQuotes(String typeofCrime) {
        ArrayList<Forms> list = new ArrayList<>();
        Cursor cursor = database.rawQuery(" SELECT * FROM crime_data" + " WHERE " + column1 + " = '" + typeofCrime + "' " , null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Forms form=new Forms();
            form.setTypeofcrime(cursor.getString(1));
            form.setAddress(cursor.getString(2));
            form.setDate(cursor.getString(0));
            Log.i(TAG, form.getDate());
            Log.i(TAG, form.getAddress());
            Log.i(TAG,form.getTypeofcrime());
            list.add(form);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }


    public ArrayList<UserLocation> getlocation() {
        ArrayList<UserLocation> list = new ArrayList<>();
        Cursor cursor = database.rawQuery(" SELECT * FROM crime_data "
                ,null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UserLocation location = new UserLocation();

            if (cursor.getString(cursor.getColumnIndex("Latitude")) != null && cursor.getString(cursor.getColumnIndex("Longitude")) != null) {
                location.setCrimetype(cursor.getString(1));
                location.setLatitude(cursor.getDouble(cursor.getColumnIndex("Latitude")));
                location.setLongitude(cursor.getDouble( cursor.getColumnIndex("Longitude")));
                list.add(location);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return list;
    }
    public void insertdata(ArrayList<Forms> form){
        if (form == null) {
            Log.i("Null value" , "Form is null");

        }else {
            for (Forms fom : form) {
                Cursor cursor = database.rawQuery(" SELECT * FROM crime_data" + " WHERE " + column4 + " = '" + fom.getUID() + "' ", null);
                if (cursor.getCount() <= 0) {
                    ContentValues cValues = new ContentValues();
                    Log.i("Something", fom.getDate());
                    Log.i("Something", fom.getAddress());
                    Log.i("Something", fom.getTypeofcrime());
                    cValues.put(column1, fom.getTypeofcrime());
                    cValues.put(column2, fom.getAddress());
                    cValues.put(column3, fom.getDate());
                    cValues.put(column4, fom.getUID());
                    long newRowId = database.insert(tablename, null, cValues);
                    cursor.close();
                }
            }
        }
    }
    public int[] getnumbers(String typecrime){
        Log.i(TAG,"hey i am in great");
      //  int k=0,j=0,m=0;
        int[] l=new int[3];
        Cursor cursor=database.rawQuery(" SELECT * FROM " + tablename + " WHERE " + column1 + " = '" + typecrime +"' AND " + column2 + " = 'Kathmandu'" ,null);
       // cursor.moveToFirst();
        l[0]=cursor.getCount();
        Cursor cursor1=database.rawQuery(" SELECT * FROM " + tablename + " WHERE " + column1 + " = '" + typecrime +"' AND " + column2 + " = 'Lalitpur'" ,null);
      //  cursor1.moveToFirst();
        l[1]=cursor1.getCount();
        Cursor cursor2=database.rawQuery(" SELECT * FROM " + tablename + " WHERE " + column1 + " = '" + typecrime +"' AND " + column2 + " = 'Bhaktapur'" ,null);
       // cursor2.moveToFirst();
        l[2]=cursor2.getCount();
//        while (!cursor.isAfterLast()){
//            if(cursor.getString(1)=="kathmandu"){
//                k++;
//            }else if (cursor.getString(1)=="Lalitpur"){
//                j++;
//            }else {
//                m++;
//            }
//        }
//        l[0]=k;
//        l[1]=j;
//        l[2]=m;
        Log.i(TAG,Integer.toString(l[0]) + "kath");
        Log.i(TAG,Integer.toString(l[1]) + "Bhak");
        Log.i(TAG,Integer.toString(l[2]) + "Lalit");
      return l;

    }
}
