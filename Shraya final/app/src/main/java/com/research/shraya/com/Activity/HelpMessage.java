package com.research.shraya.com.Activity;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.research.shraya.com.Models.Forms;
import com.research.shraya.com.Models.UserLocation;
import com.research.shraya.com.Models.formlistadapter;
import com.research.shraya.com.Models.messagedapter;
import com.research.shraya.com.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HelpMessage extends Fragment implements  messagedapter.onbuttonclickedlistener {
    private ArrayList<UserLocation> list;
    private DatabaseReference reference,mDataRef,ref;
    private RecyclerView recyclerView;
    private messagedapter adapter;
    private String type;
    String user_id;
    FirebaseAuth auth;


    public HelpMessage() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_help_message, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView=(RecyclerView)view.findViewById(R.id.myRecycleviewform);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        auth=FirebaseAuth.getInstance();
        user_id=auth.getCurrentUser().getUid();
        mDataRef= FirebaseDatabase.getInstance().getReference().child("DataofHelp");
        mDataRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list = new ArrayList<UserLocation>();
                for(DataSnapshot dataSnapshot1: dataSnapshot.getChildren())
                {
                    UserLocation p = dataSnapshot1.getValue(UserLocation.class);

                        list.add(p);
                    }
                    adapter =new messagedapter(getActivity(),list);
                    recyclerView.setAdapter(adapter);
                    adapter.setonbuttonclickedlistener(HelpMessage.this);
//                    Log.i("Retrieve", p.getTypeofcrime());
//                    Log.i("Retrieve1", p.getAge());
//                    Log.i("Retrieve2", p.getSex());
                }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });
}
    @Override
    public void onbuttonclicked(int position) {
        Intent detailintent=new Intent(getContext(),MapsActivity2.class);
        UserLocation user=list.get(position);
        detailintent.putExtra("latitude",user.getLatitude());
        detailintent.putExtra("longitude",user.getLongitude());
        startActivity(detailintent);
    }
}
