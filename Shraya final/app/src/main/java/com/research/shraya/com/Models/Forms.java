package com.research.shraya.com.Models;

public class Forms extends UserLocation {
    private  String typeofcrime;
    private String Date;
    //private Location loc;
    private String address;
    private String age;
    private String Sex;
    private boolean validate;
    private String UID;
    private String name;
    private String URIs;
    private String Description;
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String getURI() {
        return URIs;
    }

    @Override
    public void setURI(String URI) {
        this.URIs = URI;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public boolean isValidate() {
        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    private String phonenumber;
    public String getTypeofcrime() {
        return typeofcrime;
    }

    public void setTypeofcrime(String typeofcrime) {
        this.typeofcrime = typeofcrime;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

//    public Location getLoc() {
//        return loc;
//    }
//
//    public void setLoc(Location loc) {
//        this.loc = loc;
//    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }
}
