package com.research.shraya.com.Models;


import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.research.shraya.com.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
public class myadapter extends RecyclerView.Adapter<myadapter.MyViewHolder> {

    Context context;
    ArrayList<Policeusers> policeusers;
    onbuttonclick onclick;
    private int value;

    public myadapter(Context context, ArrayList<Policeusers> policeusers,int i) {
        this.context = context;
        this.policeusers = policeusers;
        this.value=i;
    }
    public  interface onbuttonclick{
         void onbuttonclicked(int position);

    }
    public void setonbuttonclickedlistener(onbuttonclick onbutton){
        onclick=onbutton;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.policelist,viewGroup,false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int i) {
        myViewHolder.policename.setText(policeusers.get(i).getName());
        myViewHolder.hero.setText( policeusers.get(i).getHeropoints());
        if(value==0) {
            myViewHolder.policeage.setText( policeusers.get(i).getAge());
            myViewHolder.policebatchnumber.setText( policeusers.get(i).getBadgenumber());
            myViewHolder.policedepartment.setText( policeusers.get(i).getDepartment());
            myViewHolder.verify.setText("VERIFY");
            // Picasso.with(context).load(policeusers.get(i).getImage1()).into(myViewHolder.idimage);
            Glide.with(context)
                    .load(policeusers.get(i).getImage1())
                    .asBitmap()
                    .centerCrop()
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            myViewHolder.idimage.setImageBitmap(resource);
                        }
                    });
            if (!policeusers.get(i).isFlag()) {
                myViewHolder.verify.setVisibility(View.VISIBLE);
                myViewHolder.verified.setVisibility(View.INVISIBLE);
                // holder.onClick(position);
            } else {
                myViewHolder.verify.setVisibility(View.INVISIBLE);
                myViewHolder.verified.setVisibility(View.VISIBLE);
            }
        }
        else {
            myViewHolder.policeage.setText(policeusers.get(i).getGmail());
            myViewHolder.policebatchnumber.setText( policeusers.get(i).getSex());
            myViewHolder.policedepartment.setText( policeusers.get(i).getType());
            myViewHolder.var.setText("Gmail");
            myViewHolder.var1.setText("Type");
            myViewHolder.var2.setText("Gender");
            // Picasso.with(context).load(policeusers.get(i).getImage1()).into(myViewHolder.idimage);
            Glide.with(context)
                    .load(policeusers.get(i).getURI())
                    .asBitmap()
                    .centerCrop()
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            myViewHolder.idimage.setImageBitmap(resource);
                        }
                    });
            myViewHolder.verify.setVisibility(View.VISIBLE);
            myViewHolder.verified.setVisibility(View.INVISIBLE);
        }
    }
    @Override
    public int getItemCount() {
        return policeusers.size();
    }
    public class  MyViewHolder extends RecyclerView.ViewHolder{
        TextView policename,policeage,policedepartment,policebatchnumber,hero,verified,var1,var,var2;
        ImageView idimage;
        Button verify;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            policename=(TextView)itemView.findViewById(R.id.policename);
            policeage=(TextView)itemView.findViewById(R.id.policeage);
            policedepartment=(TextView)itemView.findViewById(R.id.policedepartment);
            policebatchnumber=(TextView)itemView.findViewById(R.id.policebatchnumber);
            verify=(Button)itemView.findViewById(R.id.verify);
            hero=(TextView)itemView.findViewById(R.id.heropoints);
            idimage=(ImageView)itemView.findViewById(R.id.cardimage);
            var=(TextView)itemView.findViewById(R.id.var);
            var1=(TextView)itemView.findViewById(R.id.var1);
            var2=(TextView)itemView.findViewById(R.id.var2);
            verified=(TextView)itemView.findViewById(R.id.verfied);
            verify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onclick!=null){
                        int position=getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            onclick.onbuttonclicked(position);
                        }
                    }
                }
            });

        }
    }
}

